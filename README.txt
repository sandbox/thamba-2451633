A bit of documentation of the Front Page Layout Manager (hereby referenced as FPLM) module for Drupal.
Why FPLM?

When starting to develop Drupal sites, the term "responsive websites" became more and more relevant. We needed to find a solution where we could serve different layouts on different devices, and at the same time have theming that worked in a "responsive" way. For us, this resulted in a javascript library called "JAM" - Javascript Adaptive Machinery. Within JAM, out layouts was defined within php arrays, and printed out to the templates as JSON. The JSON got parsed by JAM, and made the order of elements reshuffle, and various css classes got defined, based on how the config looked like. After a while, we decided php arrays was not the way to go, and we decided to write something visual that made the maintainance of the arrays unnecessary. With FPLM, changes done to the layout would be so simle to make, that both editors and Ad Operations also could get access to the tool. We could then save a lot of time sending emails regarding small changes.
How FPLM works

The module are meant to be quite self-explainable, but in the end, nothing really is explaining itself fully. So when the module is configured (see chapter for configuration), you choose a front page to edit, and you click into it, in order to edit it within the FPLM. This gets you to a view that are showing the layouts that are available for your page.  The concept is pretty much WYSIWYG, but the appearance of widgets are not present. And the order of nodes are not possible to change within FPLM in the current version. This is because the point of the module is pretty much to define the layout, and define which styling rules to apply to articles in various postions. Nodeflow is currently used for reordering nodes, but a future verion of FPLM might possibly contain this feature.

To add new articles, simply click the "new article" buttons. You can either add articles to the top of the page, or to the bottom. Articles are automatically placed on a new row, so in order to make an article follow regular floating, you´ll have to select "automatic float" from the dropdown menu within the article position. The dropdown menus within also lets you select which width to use, which styleclass to add, and to display the lead text or not. A remove button is also located within the article positions. When clicked, the position disappears on both layouts.

To add a widget or advertisement, these elements are placed within the sidebars next to the layouts. Simply add "insert", and it will be placed on top in the layout it belongs to. Ads and widgets also contain a dropdown to define width class. A remove button is also located within the widgets, in order to move the element from the layout and into the corresponding sidebar. When clicked, it only affect the current element, even when it is also located within other layouts.

IMPORTANT: For a widget to show up in the sidebar, the block used to display it need to have a css class name attached to it, via the Block Class module. In order to make a widget into an advertisement, the classname needs to start with "adv-", and be available within the JAM DFP Manager settings. This will most likely be improved at a point, as it is not an optimal way to differ between "ads" and other types of widgets.  
Configuration

To configure FPLM, go to the configuration view. There main things you edit within this part, is image aspects, and css classes, with corresponding nicenames. You also decide what region to place blocks within when the page loads (e.g. "content_bottom_hidden", which are typically a hidden divider).

The field "Article font styles" can be filled with many different things, according to the needs. Here are an example that allow two different styles for article positions (used on topp.no);

    Default styling|styleclass-default

    Overlay|styleclass-overlay

The field named "Width styles" can be filled with whatever fits the need of the site FPLM are applied to. Here are the ones we decided to use, based on out basetheme structure;

    1/1|width-1-of-1

    1/2|width-1-of-2

    1/3|width-1-of-3

    2/3|width-2-of-3

    1/4|width-1-of-4

    2/4|width-2-of-4

    3/4|width-3-of-4

    * NB|width-netboard

    * NL|width-next-to-netboard

    * FW|width-full

Future development

If we get the time for it, we will try to make the FPLM module open source. We will also try to look at creating a better integration with Nodeflow, so you potentially can use FPLM to edit the Nodeflow queue. We also have plans to make the DFP Manager admin more visual, in order to avoid making silly mistakes in that module as well (this module currently require you to input correct formatted JSON). Another goal, is to make a similar editor that allow us to reshuffle article layout in the same way, and to integrate these tools together as a full responsive site layout management tool.
Technical aspects (frontend)

FPLM basically does one task. It manipulates the global object Drupal.settings.frontpage_layout_manager, and saves it back to the database. The whole thing is therefor built around manipulating this object, and presenting the data visually at the same time.

The frontend of FPLM is mainly written in about 850 lines of code of plain jQuery. Unfortunately, there are no unit tests written for the tool. (sad)

Every aspect of the javascript is wrapped within window.fm, which is having several child objects containing groups of similar type of functionality. This is a brief explanation of the various parts.

    fm.utils: Utilities object. Used for basic stuff, like getting creating an identifier, or finding a needle in a haystack.
    fm.templates: Templates object, used for templating. Not using handlebars, mustache or similar, but are still quite easy and viewable.

    fm.actions: Actions object. Used for templating behavior, processing actions like creating a new item, deleting something etc.

The whole thing basically initiates itself at document.ready, by running fm.actions.start(), which when fired does stuff like drawing the layouts and setting up click events for the rest of the application.
Technical aspect (backend)

Info to be written. To be updated!

 

 

 

 

