<?php
/**
 * @file
 * Rules implementation for frontpage layout manager.
 */

/**
 * Implements hook_rules_event_info().
 */
function frontpage_layout_manager_rules_event_info() {
  return array(
    'frontpage_layout_manager_save_page' => array(
      'module' => 'frontpage_layout_manager',
      'group' => t('Frontpage Layout Manager'),
      'label' => t('When saving a Frontpage layout'),
      'variables' => array(
        'fpkey' => array(
          'type' => 'text',
        )
      ),
    ),
  );
}

function frontpage_layout_manager_rules_condition_info() {
  return array(
    'frontpage_layout_manager_select_page' => array(
      'module' => 'frontpage_layout_manager',
      'group' => t('Frontpage Layout Manager'),
      'label' => t('Is frontpage layout'),
      'parameter' => array(
        'fpkey' => array(
          'type' => 'text',
          'label' => t('Layout'),
          'options list' => 'get_frontpage_layouts'
        )
      )
    )
  );
}
/**
 * @return array
 */
function get_frontpage_layouts() {
  $settings = array_keys(variable_get('frontpage_layout_manager_settings'));
  $select = array();
  foreach ($settings as $name) {
    $select[$name] = $name;
  }
  return $select;
}