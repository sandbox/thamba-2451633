<?php
/**
 * @file
 * Configuration forms.
 */

function frontpage_layout_manager_configuration_create_form($form, $state) {
  
  $views = views_get_all_views();
 
  $views_options = array();
  foreach ($views as $view) {
    $views_options[$view->name] = !empty($view->human_name) ? $view->human_name : $view->name;
  }
  
  $form = array();

  $form['manager'] = array(
    '#type' => 'fieldset',
    '#title' => t('Create new frontpage layout manager'),
    '#prefix' => '<div id="frontpage-layout-manager-ajax-content">',
    '#suffix' => '</div>',
  );

  $form['manager']['layout_view'] = array(
    '#type' => 'select',
    '#title' => 'View used to generate the frontpage?',
    '#options' => $views_options,
    '#required' => TRUE,
    //'#default_value' => @$state['values']['layout_view'],
    '#ajax' => array(
      'callback' => 'ajax_view_callback',
      'wrapper' => 'view-display-select'
    ),
  );


  if (!empty($state['values']['layout_view'])) {
    $layout_view = $state['values']['layout_view'];
  }

  $page_options = array();
  if (!empty($layout_view)) {
    foreach ($views[$layout_view]->display as $name => $page) {
      $page_options[$name] = $page->display_title;
    }
  }

  $form['manager']['layout_view_display'] = array(
    '#title' => 'View display',
    '#type' => 'select',
    '#options' => $page_options,
    '#required' => TRUE,
    '#description' => t('Select the display from the view.'),
    '#prefix' => '<div id="view-display-select">',  
    '#ajax' => array(
      'callback' => 'ajax_view_display_callback',
      'wrapper' => 'view-display-name'
    )
  );

  $name = '';
  if (!empty($layout_view)) {
    $name = str_replace(' ', '_',  $layout_view);
    if (!empty($state['values']['layout_view_display'])) {
      $layout_view_display = $state['values']['layout_view_display'];
      $name .= '_' . str_replace(' ', '_',  $layout_view_display);
    }
  }

  $form['manager']['name'] = array(
    '#title' => t('Name'),
    '#type' => 'textfield',
    '#default_value' => $name,
    '#description' => t('Machine name of this manager.<p>' . $name . '</p>'),
    '#required' => TRUE,
    '#prefix' => '<div id="view-display-name">',
    '#suffix' => '</div>',
  );

  
  $image_formats = '';
  if (!empty($layout_view)) {
    if (!empty($layout_view_display)) {
      $display = $layout_view_display;
    }
    else {
      $display = 'default';
    }
    
    if (empty($views[$layout_view]->display[$display]->display_options['fields'])) {
      $display = 'default';
    }
    
    foreach ($views[$layout_view]->display[$display]->display_options['fields'] as $field) {
      if (isset($field['type']) && 'image' == $field['type']) {
        $image_formats .= $field['field'] . '|' . $field['settings']['image_style'] . PHP_EOL;
      }
    }  
  }

  $form['manager']['image_formats'] = array(
    '#type' => 'textarea',
    '#title' => t('Article image formats'),
    '#default_value' => $image_formats,
    '#description' => t('Specify the image formats used on your frontpage.  One per line (friendly-name|css-class).  In reality these map friendly names to CSS classes and not values from Images styles.<p>' . nl2br($image_formats) . '</p>'),  
    '#suffix' => '</div>',
  );

  $form['manager']['css_path'] = array(
    '#title' => t('CSS Path'),
    '#type' => 'textfield',
    '#description' => t('Path to the frontpage CSS file containing relevant styles.'),
  );

  $form['manager']['font_styles'] = array(
    '#type' => 'textarea',
    '#title' => t('Article font styles'),
    '#description' => t('Specify the font styles used on your homepage.  One per line (friendly-name|css-class).'),  
  );
  
  $form['manager']['width_styles'] = array(
    '#type' => 'textarea',
    '#title' => t('Article width styles'),
    '#description' => t('Specify the width styles used for articles on your homepage.  One per line (friendly-name|css-class).'),
    '#required' => TRUE,
  );

  //print_r(system_rebuild_theme_data());
  $form['manager']['block_region'] = array(
    '#type' => 'select',
    '#title' => 'Which block region to fetch blocks from?',
    '#options' => system_region_list(variable_get('theme_default')),
    '#required' => TRUE,
  );
  
  $settings = variable_get('frontpage_layout_manager_settings', array());
  if (!empty($settings)) {
    $options = array('' => '-- none --');
    foreach ($settings as $k => $v) {
      $options[$k] = $k;
    }
    $form['manager']['copy_layout'] = array(
      '#type' => 'select',
      '#title' => 'Copy layout from',
      '#options' => $options,
      '#required' => FALSE,
    );
  }

  $form['manager']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save')
  );

  return $form;
}

function ajax_view_callback($form, $state) {
  return array($form['manager']['layout_view_display'], $form['manager']['name'], $form['manager']['image_formats']);
}

function ajax_view_display_callback($form, &$state) {
  return array($form['manager']['name']);
}

function frontpage_layout_manager_configuration_create_form_validate($form, &$state) {
  
  // check if the view/ display combo is already in use - if we are creating a new
  $settings = variable_get('frontpage_layout_manager_settings');
  $key = $state['values']['name'];
  
  if (isset($settings[$key])) {
    form_set_error('name', 'A frontpage manager already exists for this view/display name.');
  }
}

function frontpage_layout_manager_configuration_create_form_submit($form, &$state) {
    
  $settings = variable_get('frontpage_layout_manager_settings');
 
  $key = $state['values']['name'];
  $settings[$key] = $state['values'];
  
  // Remove Drupal form info
  unset($settings[$key]['submit']);
  unset($settings[$key]['form_build_id']);
  unset($settings[$key]['form_token']);
  unset($settings[$key]['form_id']);
  unset($settings[$key]['form_id']);
  unset($settings[$key]['op']);
  
  // Copy layout
  if (isset($state['values']['copy_layout'])) {
    $layout = db_query("select * from {frontpage_layout_manager_pages} where fpkey = :key order by pid desc limit 1", array(':key' => $state['values']['copy_layout']))->fetchAssoc();
    if (!empty($layout)) {
      global $user;
      
      $pid = db_insert('frontpage_layout_manager_pages')
        ->fields(array(
          'fpkey' => $key,
          'uid' => $user->uid,
          'layout' => $layout['layout'],
          'date' => time()
        ))
        ->execute();
    }
    
    unset($settings[$key]['copy_layout']);
  }

  variable_set('frontpage_layout_manager_settings', $settings);
  drupal_set_message(t('The settings have been saved.'), 'status');
}

function frontpage_layout_manager_configuration_edit_form_submit($form, &$state) {
  frontpage_layout_manager_configuration_create_form_submit($form, $state);
}

function frontpage_layout_manager_configuration_edit_form($form, $state, $fpkey) {
  
  $settings = variable_get('frontpage_layout_manager_settings', array());
  if (!array_key_exists($fpkey, $settings)) {
    drupal_set_message(t('Invalid page name given.', 'error'));
    drupal_goto('admin/config/system/layout-manager');
  }
  
  $settings = $settings[$fpkey];
  $state['values']['layout_view'] = $settings['layout_view'];
  $state['values']['layout_view_display'] = $settings['layout_view_display'];

  $form = frontpage_layout_manager_configuration_create_form($form, $state);
    
  $form['manager']['#title'] = 'Edit layout manager - ' . $fpkey;  
  $form['manager']['layout_view']['#value'] = $settings['layout_view'];
  $form['manager']['layout_view']['#disabled'] = TRUE;
  $form['manager']['layout_view_display']['#value'] = $settings['layout_view_display'];
  $form['manager']['layout_view_display']['#disabled'] = TRUE;
  $form['manager']['name']['#value'] = $settings['name'];
  $form['manager']['name']['#disabled'] = TRUE;
  $form['manager']['image_formats']['#default_value'] = $settings['image_formats'];  
  $form['manager']['css_path']['#default_value'] = $settings['css_path'];  
  $form['manager']['font_styles']['#default_value'] = $settings['font_styles'];
  $form['manager']['width_styles']['#default_value'] = $settings['width_styles'];
  $form['manager']['block_region']['#default_value'] = $settings['block_region'];
  
  unset($form['manager']['copy_layout']);
  
  $form['manager']['submit'] = array(
    '#type' => 'submit',
    '#submit' => array('frontpage_layout_manager_configuration_edit_form_submit'),
    '#value' => t('Edit')
  );
  
  return $form;
}

function frontpage_layout_manager_list_managers() {
  //
  $settings = variable_get('frontpage_layout_manager_settings', array());
  if (empty($settings)) {
    drupal_goto('admin/config/system/layout-manager/create');
  }

  $rows = array();
  foreach ($settings as $key => $value) {
    $row = array();
    $row[] = l($key, 'admin/structure/layout-manager/' . $key);;
    $row[] = l(t('View'), 'admin/structure/layout-manager/' . $key);
      if (user_access('administer layout manager')) {
        $row[] =  l(t('Edit'), 'admin/config/system/layout-manager/edit/' . $key);
      }
    $rows[] = $row;
  }
  
  $table = array(
    'header' => array(
      array('data' => 'Layout'), 
      array('data' => 'Operations', 'colspan' => 2)
    ), 
    'rows' => $rows
  );

  return array('#markup' => theme('table', $table));
}

function frontpage_layout_manager_revert_confirm($form_state, $build, $layout_pid) {

  $form = array();
  $form['layout_pid'] = array(
    '#type' => 'value',
    '#value' => $layout_pid
  );

  return confirm_form($form, t('Are you sure you want to revert back to this layout'), 'admin/structure/layout-manager');
}

function frontpage_layout_manager_revert_confirm_submit($form, &$form_state) {

  if ($form_state['values']['confirm']) {
    $layout_pid = abs((int) $form_state['values']['layout_pid']);

    if ($layout_pid > 0) {

      global $user;

      $row = db_query("select * from {frontpage_layout_manager_pages} where pid = :pid", array(':pid' => $layout_pid))->fetchAssoc();

      $layout = unserialize($row['layout']);
      global $LAYOUT_TYPES;
      $default = array_shift($LAYOUT_TYPES);
      $size = count($layout->$default);
      
      $save_message = $row['save_message'];

      $pid = frontpage_layout_manager_frontpage_save($row['fpkey'], $user->uid, $layout, $save_message, $size);

      drupal_set_message(t('The layout has been reverted.'));
      drupal_goto('admin/structure/layout-manager/' . $row['fpkey']);
    } else {
      drupal_set_message(t('Unable to revert to previous version, ID supplied is not an integer.'), 'error');
      drupal_goto('admin/structure/layout-manager/');
    }
  }
}
