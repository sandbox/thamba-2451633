(function ($) {
  $(window).unload(function() { 
    var url = Drupal.settings.frontpage_layout_manager.lock_release_path;
    $.ajax({
      url: url,
      type: "GET",
      success: function (data) {
        console.log('success');
      }
    });
  });
}(jQuery));
