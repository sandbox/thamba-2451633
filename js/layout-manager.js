(function($){


  /** 
   * A wrapper object to contain our data and application logic
   */
  window.fm = {
    
    
    /**
     * Utilities
     */
    utils: {
    
      // Default item
      get_defaultitem: function(){
        
        // Default item to insert
        var defaultitem = {
          "image_format": "field_image",
          "type": "node",
          "hide_lead": false,
          "in_layout": true,
          "styleclass": null,
          "fontsizeclass": null,
          "floatoptions": "first",
          "hidden": true,
          "itemidentifier": fm.utils.makeid(), 
          "title": "Tittel",
          "ingress": "Ingress",
          "width": "width-1-of-1",
          "image": "http://placekitten.com/320/180"
        };
        
        // Return the object
        return defaultitem;
        
      },
      
      // Make a random hash of 5 characters
      makeid: function(){
        var text = "rand-";
        var possible = "abcdefghijklmnopqrstuvwxyz0123456789";
        for( var i=0; i < 5; i++ ) text += possible.charAt(Math.floor(Math.random() * possible.length));
        return text;
      },
      
      // Extract classname from string, based on pattern. Returns false if no match
      extract: function(haystack, needle){
        var haystack = haystack || '';
        var classes = haystack.split(' ');
        var classname = false;
        i = 0;
        while (i < classes.length) {
          if (classes[i].indexOf(needle) !== -1) {
            classname = classes[i];
          } i++;
        }
        return classname;
      },
      
      // An array of ramdom identifiers, used to give a corresponding class name to each article
      random_identifiers: function(){
        if (!fm.utils.randomarray){
          fm.utils.randomarray = [];
          for (var i=0; i<1000; i++){
            fm.utils.randomarray[i] = fm.utils.makeid();
          }
        }
        return fm.utils.randomarray;
      },
      
      // Get info about a given ad, based on ad_identifier and layoutname
      get_ad_info: function(ad_identifier, layoutname){
        
        // @TODO: remove this some time in the future. We need this in order to map the definitions used by JAM. 
        layoutname = (layoutname == 'smartphone_portrait') ? 'mobile': layoutname;
        
        // Create object, that says we bu default missed fetching something interesting. 
        var info = { existance: false };
        
        // Check that we are actually checking against an ad
        if (fm.utils.extract(ad_identifier, 'adv-') !== false){
        
          // Get info found within blocks
          $.each(Drupal.settings.frontpage_layout_manager.blocks, function(key, item){
            if (fm.utils.extract(key, ad_identifier) !== false){
              info.ad_name = fm.utils.extract(key, 'adv-').replace('adv-', ''); // The ad name, without adv-
              info.contentelementselector = ad_identifier; 
              info.image = 'http://pagead2.googlesyndication.com/simgad/8689375666247397509';
              info.type = 'advertorial';
            }
          });
          
          // Get info found within ad config
          $.each(Drupal.settings.frontpage_layout_manager.adverts.slotDefinitions, function(key, value){
            if (fm.utils.extract(key, info.ad_name) == info.ad_name){
              if (typeof value.layoutSizes[layoutname] !== 'undefined') {
                info.size = value.layoutSizes[layoutname];
                info.existance = true;
                info.frontpage = value.frontpage;
                info.title = info.ad_name + ' (' + info.size + ')';
                
                // Default info to add, when placed to a palette
                info.width = 'width-1-of-1';
                info.floatoptions = 'first';
              } 
            }
          });
        
        }
        
        return info;
        
      },
      
      // Get info about a given widget, based on contentelementselector
      get_widget_info: function(contentelementselector){
        
        // Create object, that says we bu default missed fetching something interesting. 
        var info = { existance: false };
        
        // Get info found within blocks
        $.each(Drupal.settings.frontpage_layout_manager.blocks, function(key, description){
        
          // Check the block name for matches
          if (fm.utils.extract(key, contentelementselector) !== false){
            info.existance = true;
            info.selector = key;
            info.contentelementselector = key;
            info.description = description;
            info.type = 'widget';
                
            // Default info to add, when placed to a palette
            info.width = 'width-1-of-1';
            info.floatoptions = 'first';
          }
        });
        
        return info;
        
      },
      
    },
    
    
    /**
     * Actions
     */
    actions: {
      
      
      /**
       * Write all items to a JSON object we want the server to store (new version).
       */
      collect_definitions: function(layouts){
        var results = {};
        
        // Collect all info about any boz element within the requested layouts
        $.each(layouts, function(key, layout){
          results[layout] = [];
          
          $('.' + layout + '_layout .wrap .grid-box').each(function(i){
            
            // Create object to store data within
            results[layout].push({ position: i+1 });
            
            // All the classes applied to the element
            var all_classes = $(this).attr('class');
            
            // Extract "first" or "last" from classes
            if (fm.utils.extract(all_classes, 'first') !== false || fm.utils.extract(all_classes, 'last') !== false){
              results[layout][i]['floatoptions'] = (fm.utils.extract(all_classes, 'first') !== false) ? 'first' : 'last';             
            } else {
              results[layout][i]['floatoptions'] = null;
            }
            
            // Extract width from the classes
            results[layout][i]['width'] = (fm.utils.extract(all_classes, 'width-') !== false) ? fm.utils.extract(all_classes, 'width-') : null;
            
            // Extract css style classes, by extracting "styleclass-"
            results[layout][i]['styleclass'] = (fm.utils.extract(all_classes, 'styleclass-') !== false) ? fm.utils.extract(all_classes, 'styleclass-') : null;
            
            // Extract hide-lead from the classes
            results[layout][i]['hide_lead'] = (fm.utils.extract(all_classes, 'hide-lead') == 'hide-lead') ? true : false;
            
            // Extract image format from the classes
            results[layout][i]['image_format'] = (fm.utils.extract(all_classes, 'field_image') !== false) ? fm.utils.extract(all_classes, 'field_image') : 'field_image';
            
            // Extract font size class from the classes (though, the font size option is not yet implemented)
            results[layout][i]['fontsizeclass'] = (fm.utils.extract(all_classes, 'fontsize-') !== false) ? fm.utils.extract(all_classes, 'fontsize-') : null;
            
            // Extract type (should be either node, advertorial or widget)
            if (fm.utils.extract(all_classes, 'node') == 'node') results[layout][i]['type'] = 'node';
            else if (fm.utils.extract(all_classes, 'advertorial') == 'advertorial') results[layout][i]['type'] = 'advertorial';
            else if (fm.utils.extract(all_classes, 'widget') == 'widget') results[layout][i]['type'] = 'widget';
            else results[layout][i]['type'] = null; // Cannot see how this should be the case, but just to be sure. 
            
            // Extract css contentElement selector if data('contentelementselector')
            results[layout][i]['contentelementselector'] = $(this).data('contentelementselector');
            
            // Make sure the item is placed within the layout
            results[layout][i]['in_layout'] = true;
            
          });
        });
        return results;
      },
      
    
      /**
       * Update number count and actual content within article blocks
       */
      update_article_data: function(){
        $.each(Drupal.settings.frontpage_layout_manager.layout_types, function(key, layoutname){
          $('.' + layoutname + '_layout .grid-box span:contains("node")').each(function(key, item){
            
            // Configure the element and content that goes in to it
            var parentEl = $(this).parent().parent();
            var imagemarkup = $('.content img', parentEl);
            var titlemarkup = $('.textwrap h2', parentEl);
            var ingressmarkup = $('.textwrap p.ledtext', parentEl);
            var image_format_string = (fm.utils.extract(parentEl.attr('class'), 'field_image') !== false) ? fm.utils.extract(parentEl.attr('class'), 'field_image') : 'field_image';
            var title = fm.utils.get_defaultitem().title;
            var ingress = fm.utils.get_defaultitem().ingress;
            var new_image = fm.utils.get_defaultitem().image;
            
            // Then, check if we have some real content to display
            if (typeof Drupal.settings.frontpage_layout_manager.content[key] !== 'undefined') {
              new_image = (typeof Drupal.settings.frontpage_layout_manager.content[key].images[image_format_string] !== 'undefined') ? Drupal.settings.frontpage_layout_manager.content[key].images[image_format_string] : new_image;
              title = (typeof Drupal.settings.frontpage_layout_manager.content[key].title !== 'undefined') ? Drupal.settings.frontpage_layout_manager.content[key].title : title;
              ingress = (typeof Drupal.settings.frontpage_layout_manager.content[key].ingress !== 'undefined') ? Drupal.settings.frontpage_layout_manager.content[key].ingress : ingress;
            } 
            
            // Replace the content
            imagemarkup.attr('src', new_image);
            titlemarkup.html(title);
            ingressmarkup.html(ingress);
            
            // Update counter
            $(this).html("node (" + (key+1) + ')');
          });
        });
      },


      /**
       * Click handlers setup on dom ready
       */
      setup_dom_ready_click_handlers: function(){
      
        // Clicking tips palette hides the tips
        $('.tips_palette h3').on('click', function(){
          $('p', $('.tips_palette')).slideToggle();
        });
        
        // Select handler for the dropdowns that define image aspect ration
        $('#edit-submit').on("click", function(event){
          
          // Disable default
          event.preventDefault();
          
          // Collect results
          var results = fm.actions.collect_definitions(Drupal.settings.frontpage_layout_manager.layout_types);
          
          // Timer used before form is populated with data until it gets submitted
          var submit_timer = 500;
          
          // Check if somebody wants to debug
          if (location.href.match(/(\?|&)debug($|&|=)/)){
            
            // Show textarea we write the results object to
            $('#edit-json-code:hidden').slideToggle(500); 
            
            // Make sure we do not submit the form right away
            submit_timer = 5000; 
            
          }
          
          // Write JSON object to the text area
          $('#edit-json-code').val(JSON.stringify(results));
          
          // Submit form after amount of milliseconds defined within the submit_timer variable
          setTimeout(function(){
            $('#frontpage-layout-manager-form').submit();
          }, submit_timer);
          
        });
        
        // Add articles to the grid(s) on button click
        $('.article_palette .palette-element').on("click", function(){
          
          // Variable to define weather to add to top or bottom
          var add_to_top = ($(this).hasClass('addtotop'));
          
          // Get default item
          var defaultitem = fm.utils.get_defaultitem();
          
          // Append/prepend to layout
          $.each(Drupal.settings.frontpage_layout_manager.layout_types, function(key, layoutname){
            defaultitem.width = (layoutname == 'desktop') ? "width-1-of-3" : "width-1-of-1";
            if (add_to_top){
              $('.' + layoutname + '_layout .wrap').prepend(fm.templates.boxtemplate(defaultitem));
            } else {
              $('.' + layoutname + '_layout .wrap').append(fm.templates.boxtemplate(defaultitem));
            }
          });
          
          // Show item
          $('*[data-random_id="' + defaultitem.itemidentifier + '"]').show(400);
        
          // Draw numbers within article headers
          setTimeout(function(){ fm.actions.update_article_data(); }, 400)
        
          // Initiate the element, so it behaves as other boxes
          fm.actions.setup_box_behaviour('*[data-random_id="' + defaultitem.itemidentifier + '"]');
          
        });
        
        // Gui options
        $(".togglegui").change(function() {
        
          // Option for hiding headers
          if ($(this).hasClass('hide_headers')){
            if(this.checked) {
              $('.header').css({ 'display': 'none' });
            } else {
              $('.header').css({ 'display': 'block' });
            }
          }
        
          // Option for hiding mobile version
          if ($(this).hasClass('hide_mobile')){
            if(this.checked) {
              $('.smartphone_portrait_layout').hide(500);
            } else {
              $('.smartphone_portrait_layout').show(500);
            }
          }
        
          // Option for hiding mobile version
          if ($(this).hasClass('hide_desktop')){
            if(this.checked) {
              $('.desktop_layout').hide(500);
            } else {
              $('.desktop_layout').show(500);
            }
          }
          
        });
        
        // Style hack, because this was a bit hard to style by css.
        if ($('#console').is(':visible')){
          $('#edit-actions').css({ 'top': '-125px' })
        } 
        
      },
    
    
      /**
       * Box setup, makes boxes behave as expected. 
       */          
      setup_box_behaviour: function(element){
        
        
        // Show options when hovering a box within a layout
        $(element).hover(function() { $('.hoverplaceholder', this).show(); }, function() { $('.hoverplaceholder', this).hide(); });
        
        
        // Select handler for the dropdowns that define item widths
        $('select.width_dropdown', element).on("change", function(){
          
          var targetEl = $(this).parent().parent();
          targetEl.removeClass(fm.utils.extract(targetEl.attr('class'), 'width-'));
          targetEl.addClass(this.value);
          
        });
        
        
        // Select handler for the dropdowns that define float options
        $('select.floatoptions_dropdown', element).on("change", function(){
          
          var targetEl = $(this).parent().parent().parent().parent();
          targetEl.removeClass(fm.utils.extract(targetEl.attr('class'), 'first'));
          targetEl.addClass(this.value);
          
        });
        
        
        // Select handler for the dropdowns that define float options
        $('select.styleclass_dropdown', element).on("change", function(){
          
          var targetEl = $(this).parent().parent().parent().parent();
          targetEl.removeClass(fm.utils.extract(targetEl.attr('class'), 'styleclass-'));
          targetEl.addClass(this.value);
          
        });
        
        
        // Select handler for the dropdown that define lead text visibility
        $('select.leadtext_dropdown', element).on("change", function(){
          
          var targetEl = $(this).parent().parent().parent().parent();
          targetEl.removeClass(fm.utils.extract(targetEl.attr('class'), '-lead'));
          targetEl.addClass(this.value);
          
        });
        
        
        // Select handler for the dropdowns that define image aspect ration
        $('select.imageaspect_dropdown', element).on("change", function(){
          
          // Variables
          var targetEl = $(this).parent().parent().parent().parent();
          var imgSrc = $('.content img', targetEl).attr('src');
          var layoutname = $(this).closest('.layout');
          var position = $(".grid-box.node", layoutname ).index(targetEl);
          var kittehsize = this.value.split('_'); // If we do not find an image to present, we place kitteh
          var image = (Drupal.settings.frontpage_layout_manager.content[position].images[this.value]) ? Drupal.settings.frontpage_layout_manager.content[position].images[this.value] : 'http://placekitten.com/g/' + (kittehsize[2] * 100) + '/' + (kittehsize[3] * 100);
          
          // Change image
          $('.content img', targetEl).attr('src', image);
          
          // Change class
          if (fm.utils.extract(targetEl.attr('class'), 'field_image') !== false){
            targetEl.removeClass(fm.utils.extract(targetEl.attr('class'), 'field_image'));
          }
          targetEl.addClass(this.value);
          
        });
        
        
        // Click handler for removing elements
        $('.remove', element).on("click", function(){
          var self_reference = this;
          var parentEl = $(self_reference).parent().parent().parent().parent();
          var identifier = parentEl.data('random_id');
          
          if (identifier.indexOf('advertorial') > 0 || (identifier.indexOf('widget') > 0)){
          
            // Special handling for advertorials and widgets
            $('.hoverplaceholder', parentEl).hide();
            $('*[data-random_id="' + identifier + '"]').hide(400);
            
            $.each(Drupal.settings.frontpage_layout_manager.layout_types, function(key, layoutname){
              if ($(self_reference).parents('.' + layoutname + '_layout').length > 0){
                setTimeout(function(){ 
                  $('.' + layoutname + '_palette .wrap').prepend(parentEl);
                },405);
              }
            });
            
            setTimeout(function(){ 
              $('*[data-random_id="' + identifier + '"]').slideToggle(200); 
            }, 410);
            
          } else {
          
            // Default handling (node/articles)
            $('*[data-random_id="' + identifier + '"]').hide('slow');
            setTimeout(function(){ $('*[data-random_id="' + identifier + '"]').remove() }, 500);
        
            // Draw numbers within article headers
            setTimeout(function(){ fm.actions.update_article_data(); }, 600);
            
          }
        });
        
        
        // Click handler for inserting elements
        $('.insert', element).on("click", function(){
          var parentEl = $(this).parent().parent().parent().parent();
          var self_reference = this;
          parentEl.hide();
          $('.hoverplaceholder', parentEl).hide();
          
          $.each(Drupal.settings.frontpage_layout_manager.layout_types, function(key, layoutname){
            if ($(self_reference).parents('.' + layoutname + '_palette').length > 0){
              $('.' + layoutname + '_layout .wrap').prepend(parentEl);
            }
          });
          parentEl.show(400);
        });
        
        
      },
    
      
      /**
       * Reads from the stored JSON object, arranges boxes into layouts or places things in corresponding palette. 
       */
      arrange_layout: function(layoutname){
        var articlecounter = 0; 
        
        if (typeof Drupal.settings.frontpage_layout_manager.layouts[layoutname] !== 'undefined'){
        
          // Loop through all gridElDefinitions to see if we find ads or widgets
          $.each(Drupal.settings.frontpage_layout_manager.layouts[layoutname], function(key, item){
          
            // Match hits advertorial or widget (the contentelementselector is not used for node objects)
            if (typeof item.contentelementselector !== 'undefined'){
              
              item.contentelementselector = item.contentelementselector.replace('.', ''); 
              
              // Check if we find adv- within block text
              if (fm.utils.extract(item.contentelementselector, 'adv-') !== false){
               
                // Matched type advertorial
                var ad_info = fm.utils.get_ad_info(fm.utils.extract(item.contentelementselector, 'adv-'), layoutname);
                if (ad_info.existance){
                  item.image = ad_info.image;
                  item.in_layout = true;
                  item.title = ad_info.title;
                  item.contentelementselector = ad_info.contentelementselector;
                  item.description = '.' + ad_info.contentelementselector;
                }
                
              } else {
              
                // Matched type widget
                item.selector = fm.utils.get_widget_info(item.contentelementselector).selector;
                item.contentelementselector = fm.utils.get_widget_info(item.contentelementselector).contentelementselector;
                item.description = fm.utils.get_widget_info(item.contentelementselector).description;
                item.in_layout = true;
                
              }
              
            }
           
          });
          
            
          // Loop through all blocks
          $.each(Drupal.settings.frontpage_layout_manager.blocks, function(key, item){
            
            if (fm.utils.extract(key, 'adv-') == false){
            
              // Widget spesific
              item = fm.utils.get_widget_info(key); 
              item.in_layout = false;
              
              // Look for the widget, add to gridElDefinitions if it do not have the in_layout attribute set to true
              $.each(Drupal.settings.frontpage_layout_manager.layouts[layoutname], function(key, gridEl){
                if (typeof gridEl.contentelementselector !== 'undefined'){
                  if (gridEl.contentelementselector == item.contentelementselector){
                    if (gridEl.in_layout){
                      item.in_layout = true;
                    }
                  }
                }
              });
              
              // Push to the layouts array
              if (!item.in_layout){
                Drupal.settings.frontpage_layout_manager.layouts[layoutname].push(item);
              }
              
            } else {
            
              // Ad (DFP) spesific
              item = fm.utils.get_ad_info(key, layoutname);
              item.description = '.' + item.contentelementselector;
              item.in_layout = false;
              
              // Look for the widget, add to gridElDefinitions if it do not have the in_layout attribute set to true
              $.each(Drupal.settings.frontpage_layout_manager.layouts[layoutname], function(key, gridEl){
                if (typeof gridEl.contentelementselector !== 'undefined'){
                  if (item.contentelementselector == gridEl.contentelementselector){
                    if (gridEl.in_layout){
                      item.in_layout = true;
                    }
                  }
                }
              });
              
              // Push to the layouts array
              if (item.existance && !item.in_layout) {
                Drupal.settings.frontpage_layout_manager.layouts[layoutname].push(item);
              }
              
            }
            
          }); 
          
          // Place all server stored items into the layout or palette
          $.each(Drupal.settings.frontpage_layout_manager.layouts[layoutname], function(key, item){
            if (fm.utils.extract(item.type, 'node') == 'node') item.itemidentifier = fm.utils.random_identifiers()[articlecounter] + '-node';
            if (fm.utils.extract(item.type, 'advertorial') == 'advertorial') item.itemidentifier = fm.utils.random_identifiers()[Math.floor((Math.random()*999)+1)] + '-advertorial';
            if (fm.utils.extract(item.type, 'widget') == 'widget') item.itemidentifier = fm.utils.random_identifiers()[Math.floor((Math.random()*999)+1)] + '-widget';
            if (fm.utils.extract(item.type, 'node') == 'node'){
            
              // Apply title, ingress text and image to the articles
              if (typeof Drupal.settings.frontpage_layout_manager.content == 'undefined'){
                Drupal.settings.frontpage_layout_manager.content = {};
              }
              if (typeof Drupal.settings.frontpage_layout_manager.content[articlecounter] !== "undefined"){
                item.ingress = (Drupal.settings.frontpage_layout_manager.content[articlecounter].ingress !== "undefined") ? Drupal.settings.frontpage_layout_manager.content[articlecounter].ingress : fm.utils.get_defaultitem().ingress;
                item.title = (Drupal.settings.frontpage_layout_manager.content[articlecounter].title !== "undefined") ? Drupal.settings.frontpage_layout_manager.content[articlecounter].title : fm.utils.get_defaultitem().title;
              } else {
                item.title = fm.utils.get_defaultitem().title;
                item.ingress = fm.utils.get_defaultitem().ingress;
                item.new_image = fm.utils.get_defaultitem().image;
              }              
              
              var defaultformat = Object.keys(Drupal.settings.frontpage_layout_manager.image_formats)[0];
              if (fm.utils.extract(item.image_format, 'field_image') !== false){
                
                var image_aspect = fm.utils.extract(item.image_format, 'field_image');
                var kittehsize = image_aspect.split('_'); // If we do not find an image to present, we place kitteh
                item.image = 'http://placekitten.com/' + (parseInt(kittehsize[2]) * 100) + '/' + (parseInt(kittehsize[3]) * 100);
                
                if (typeof Drupal.settings.frontpage_layout_manager.content[articlecounter] !== "undefined"){
                  if (typeof Drupal.settings.frontpage_layout_manager.content[articlecounter].images[image_aspect] !== "undefined"){
                    item.image = Drupal.settings.frontpage_layout_manager.content[articlecounter].images[image_aspect];
                  } 
                }
                
              } else {
                
                var kittehsize = defaultformat.split('_'); // If we do not find an image to present, we place kitteh
                item.image = 'http://placekitten.com/' + (parseInt(kittehsize[2]) * 100) + '/' + (parseInt(kittehsize[3]) * 100);
                
                if (typeof Drupal.settings.frontpage_layout_manager.content[articlecounter] !== "undefined"){
                  if (typeof Drupal.settings.frontpage_layout_manager.content[articlecounter].images[defaultformat] !== "undefined"){
                    item.image = Drupal.settings.frontpage_layout_manager.content[articlecounter].images[defaultformat];
                  } 
                } 
                
              }
              
            }
            if (item.in_layout) {
              $('.' + layoutname + '_layout .wrap').append(fm.templates.boxtemplate(item));
            } else {
              $('.' + layoutname + '_palette .wrap').append(fm.templates.boxtemplate(item));
            }
            if (fm.utils.extract(item.type, 'node') == 'node'){ articlecounter ++; }
          });
          
        }
      },
    
    
      /**
       * DOM ready events
       */
      start: function(){ 
        
        // Draw the views specified in our settings, apply sortable
        $.each(Drupal.settings.frontpage_layout_manager.layout_types, function(key, layoutname){ 
          fm.actions.arrange_layout(layoutname); // Draw layouts
          $('.' + layoutname + '_layout .wrap').sortable({ stop: function(evt, ui) { setTimeout(function(){ fm.actions.update_article_data(); }, 400) } }); // Apply sortable
        });
        
        // Initiate box behaviour on box elements
        fm.actions.setup_box_behaviour('.grid-box');
        
        // Setup default click handlers
        fm.actions.setup_dom_ready_click_handlers();
        
        // Draw numbers within article headers
        fm.actions.update_article_data();
        
      }
      
      
    },
    
    
    /** 
     * Template functions
     */
    templates: {
    
    
      // Box template. Writes each box
      boxtemplate: function(item){
        
        // Adding layout classes
        var cssLayoutClass = (item.floatoptions !== null) ? ' ' + item.floatoptions : '';
            cssLayoutClass+= (item.width !== null) ? ' ' + item.width : '';
        
        // Adding style classes
        var cssStyleClass = (item.styleclass !== null) ? ' ' + item.styleclass : '';
            cssStyleClass+= (item.image_format !== null) ? ' ' + item.image_format : '';
            cssStyleClass+= (item.fontsizeclass !== null) ? ' ' + item.fontsizeclass : '';
            cssStyleClass+= (item.hide_lead) ? ' hide-lead' : '';
        
        // Adding meta classes    
        var cssMetaClass = (item.type !== null) ? ' ' + item.type : '';
        
        // Data to add
        var contentelementselector = (item.contentelementselector) ? ' data-contentelementselector=".' + item.contentelementselector + '"' : '';
        var randomIdentifier = ' data-random_id="' + item.itemidentifier + '"';
        
        // Elements to add
        var dropdown_width = (item.prevent_resize) ? '' : fm.templates.width_dropdown(fm.utils.extract(cssLayoutClass, 'width-'));
        var dropdown_floatoptions = fm.templates.floatoptions_dropdown(fm.utils.extract(cssLayoutClass, 'first')) + '<br />';
        var dropdown_styleclass = (item.type == 'node') ? fm.templates.styleclass_dropdown(item.styleclass) + '<br />' : '';
        var dropdown_imageaspect = (fm.utils.extract(cssStyleClass, 'field_image') !== 'false' && item.type == 'node') ? fm.templates.imageaspect_dropdown(fm.utils.extract(cssStyleClass, 'field_image')) + '<br />' : '';
        var dropdown_leadtext = (item.type == 'node') ? fm.templates.leadtext_dropdown(item.hide_lead) + '<br />' : '';
        var button_add = '<div class="insert">Legg til</div>';
        var button_remove = '<div class="remove">Ta bort</div>';
        var image = (item.image) ? '<img src="' + item.image + '">' : '';
        var selector = (item.selector) ? '<br /><span class="selector">.' + item.selector + '</span>' : '';
        var info = (item.description) ? '<div class="gridbox-info">' + item.description + selector + '</div>' : '';
        var title = (item.title) ? '<h2>' + item.title + '</h2>' : '';
        var ingress = (item.ingress) ? '<p class="leadtext">' + item.ingress + '</p>' : '';
        
        // Inline hiding of the element (only used within this GUI for animations)
        var inline_hide_element = (item.hidden) ? ' style="display:none;"' : '';
        
        // The template
        var template = '<div class="grid-box' + cssLayoutClass + cssStyleClass + cssMetaClass + '"' + contentelementselector + randomIdentifier + inline_hide_element + '>';
            template+=   '<div class="header"><span>' + item.type + '</span>' + dropdown_width + '</div>';
            template+=   '<div class="content">' + image + info + '<div class="textwrap">' + title + ingress + '</div>';
            template+=     '<div class="hoverplaceholder">';
            template+=       '<div class="selectplaceholder">' + dropdown_floatoptions + dropdown_styleclass + dropdown_imageaspect + dropdown_leadtext + button_remove + '</div>';
            template+=       '<div class="insertplaceholder">' + button_add + '</div>';
            template+=     '</div>';
            template+=   '</div>';
            template+= '</div>';
            
        return template;
        
      },
      
      
      // Writes a dropdown that lets you specify widths of boxes
      width_dropdown: function(selected){
      
        var template = '<select class="width_dropdown">';
            $.each(Drupal.settings.frontpage_layout_manager.width_settings, function(key, value){ template+= '<option ' + ((selected == value) ? 'selected="selected" ' : '') + 'value="' + value + '">' + key + '</option>'; });
            template+= '</select>';
            
        return template;
        
      },
      
      
      // Writes a dropdown that lets you specify how boxes floats
      floatoptions_dropdown: function(selected){
      
        var template = '<select class="floatoptions_dropdown">';
            template+=   '<option selected="selected" value="">Automatisk flyt</option>';
            template+=   '<option ' + ((selected == 'first') ? 'selected="selected" ' : '') + 'value="first">Start ny rad</option>';
            template+= '</select>';
            
        return template;
        
      },
      
      
      // Writes a dropdown that lets you specify the font set used by each box
      styleclass_dropdown: function(selected){
      
        var template = '<select class="styleclass_dropdown">';
            $.each(Drupal.settings.frontpage_layout_manager.nodeclass, function(key, value){
              template+= '<option ' + ((selected == value) ? 'selected="selected" ' : '') + 'value="' + value + '">' + key + '</option>';
            });
            template+= '</select>';
            
        return template;
        
      },
      
      
      // Writes a dropdown that lets you specify if the lead text/ingress should be shown or not
      leadtext_dropdown: function(selected){
      
        var template = '<select class="leadtext_dropdown">';
            template+=   '<option ' + ((!selected) ? 'selected="selected" ' : '') + 'value="show-lead">Vis ingress</option>';
            template+=   '<option ' + ((selected) ? 'selected="selected" ' : '') + 'value="hide-lead">Skjul ingress</option>';
            template+= '</select>';
            
        return template;
        
      },
      
      
      // Writes a dropdown that lets you specify which imageaspect to use
      imageaspect_dropdown: function(selected){
      
        var template = '<select class="imageaspect_dropdown">';
            $.each(Drupal.settings.frontpage_layout_manager.image_formats, function(key, value){
              template+= '<option ' + ((selected == key) ? 'selected="selected" ' : '') + 'value="' + key + '">' + value + '</option>';
            });
            template+= '</select>';
            
        return template;
        
      }
    
    }
  
  }; // Ends global wrapper object
  
  
  /**
   * Fire it up!
   */
  $(document).ready(function(){
  
    // Returns IE version or false
    function isIE (){
      var myNav = navigator.userAgent.toLowerCase();
      return (myNav.indexOf('msie') != -1) ? parseInt(myNav.split('msie')[1]) : false;
    }
    
    // Who knows if this thing actually works in IE9. But let´s not take a bet on it. 
    if (isIE() < 10) {
    
      // Start up the layout manager
      fm.actions.start();
      
    } else {
    
      // Give a warning if somebody is trying to edit without a proper browser. 
      alert('Your browser are not supported for use within the layout manager. Please upgrade it to latest version, or come back in another browser. ');
      
    }
    
  });

})(jQuery);