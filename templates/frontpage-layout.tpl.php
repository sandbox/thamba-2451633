<div class="wrap">

  <div class="resultswrapper">
    <?php print drupal_render($form); ?>
  </div>

  <div class="left_palette">
    <div class="palettefloat">
      <?php
      if (!empty($history)):
      ?>
      <div class="palette history_palette">
        <h3>Bruk versjon</h3>
        <div class="wrap">
          <?php
          foreach ($history as $item):
          ?>
          <div class="palette-element"><span><a href="<?php print url('admin/config/system/layout-manager/revert/' . $item->pid); ?>"><?php print $item->save_message; ?></a></span></div>
          <?php
          endforeach;
          ?>
        </div>
      </div>
      <?php
      endif;
      ?>
      <div class="palette article_palette">
        <h3>Artikler</h3>
        <div class="wrap">
          <div class="palette-element first-palette-element addtotop"><span>Ny artikkel &oslash;verst</span></div>
          <div class="palette-element second-palette-element addtobottom"><span>Ny artikkel nederst</span></div>
        </div>
      </div>
      <div class="palette smartphone_portrait_palette">
        <h3>Mobilelementer</h3>
        <div class="wrap">
        </div>
      </div>
      <div class="palette optionspalette">
        <h3>Visningvalg</h3>
        <div class="wrap">
          <span><input class="togglegui hide_headers" type="checkbox" name="hide_headers" value=""> Skjul headere</span>
          <span><input class="togglegui hide_mobile" type="checkbox" name="hide_mobile" value=""> Skjul mobilversjon</span>
          <span><input class="togglegui hide_desktop" type="checkbox" name="hide_desktop" value=""> Skjul desktopversjon</span>
        </div>
      </div>
    </div>
  </div>

  <div class="right_palette">
    <div class="palettefloat">
      <div class="palette tips_palette">
        <h3>Bruksanvisning</h3>
        <p>Hver rad b&oslash;r ha et element som starter ny rad. Ny rad markeres med oransje firkant, og defineres per artikkelposisjon via nedtrekksmeny. </p>
        <p>N&aring;r du legger til/fjerner en artikkelposisjon, blir denne lagt til/fjernet p&aring; b&aring;de desktop og mobil. </p>
        <p>Annonser og widgets kan eksistere p&aring; en layout uten at de trenger &aring; v&aelig;re tilstede p&aring; den andre. </p>
        <p>Husk &aring; se n&oslash;ye over begge versjonene f&oslash;r du lagrer. </p>
      </div>
      <div class="palette desktop_palette">
        <h3>Desktopelementer</h3>
        <div class="wrap">
        </div>
      </div>
    </div>
  </div>

  <div class="layout desktop_layout">
    <h1>Desktopversjon</h1>
    <div class="wrap"></div>
  </div>

  <div class="layout smartphone_portrait_layout">
    <h1>Mobilversjon</h1>
    <div class="wrap"></div>
  </div>

</div>